import json
import pprint
import re

with open("result.json", "r") as read_file:
    data = json.load(read_file)

pp = pprint.PrettyPrinter(width=80, compact=True)
messages = data["messages"]

pattern = re.compile("(\d+) zu (\d+)")

print ("Date\tsystolic\tdiastolic")
for m in messages:
    t = m["text"]
    d = m["date"]
    # pp.pprint(t)
    if isinstance(t, str):
        if pattern.match(t):
            g = pattern.search(t)
            print (d + "\t" + g.group(1) + "\t" + g.group(2))
